const express = require('express');
const app = express();
const ejs = require('ejs');
const bodyParser = require('body-parser');

app.set('view engine', 'ejs');

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));

// Buat routing untuk masing-masing halaman
app.get('/', (req, res) => {
  res.render('index');
});

app.get('/game', (req, res) => {
  res.render('index1');
});

app.get('/gamechoice', (req, res) => {
  res.render('gamechoice');
});

app.get('/gamechoice1', (req, res) => {
  res.render('gamechoice1');
});

app.get('/las', (req, res) => {
  res.render('las');
});

app.get('/require', (req, res) => {
  res.render('require');
});

app.get('/score', (req, res) => {
  res.render('score');
});

const port = 3000; 
app.listen(port, () => {
  console.log(`Server berjalan pada http://localhost:${port}`);
});
